/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.pageobjects.sqs;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Hyperlink;

import org.openqa.selenium.By;

import lombok.Getter;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Top Navigation Page Object
 * <img src='doc-files/TopNavigationBar.jpg' alt=''>
 */
public class TopNavigationBar extends PageObject {

  @Getter
  private final Hyperlink career = new Hyperlink(By.xpath("//a[.='Career']"));

  /**
   * Instantiates a new Navigation bar.
   */
  public TopNavigationBar() {
    onPage();
  }

  public void onPage() {
    career.isDisplayed();
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  @Step("Navigating to careers page")
  public SqsCareerPortalPage navigateToCareers() {
    career.click();
    return navigatingTo(SqsCareerPortalPage.class);
  }


  /**
   * List of pages on navbar.
   * using enums can restrict user input,
   * therefore avoiding user input errors later
   */
  public enum Pages {
    /**
     * Newsroom
     */
    NEWSROOM,
    /**
     * Training
     */
    TRAINING,
    /**
     * Investors
     */
    INVESTORS,
    /**
     * Careers
     */
    CAREER,
    /**
     * Countries
     */
    COUNTRIES,
    /**
     * DE
     */
    DE
  }
}
