/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.pageobjects.sqs;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.Label;

import org.openqa.selenium.By;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Career Ireland Page Object
 * <img src='doc-files/CareerIrelandPage.jpg' alt=''>
 */
public class CareerIrelandPage extends PageObject {

  private final Hyperlink
      vacanciesAtSqs =
      new Hyperlink(By.xpath("//a[text()='Vacancies at SQS']"));
  private final Label
      growYourCareerWithSqsIreland =
      new Label(By.xpath("//h1[text()='Grow your career with SQS Ireland']"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public CareerIrelandPage() {
  }

  @Step("Verifying we are on the Career Ireland page")
  public void onPage() {
    growYourCareerWithSqsIreland.isPresent();
  }

  @Step("Navigating to Vacancies page")
  public VacanciesPage navigateToVacanciesPage() {
    vacanciesAtSqs.click();
    return navigatingTo(VacanciesPage.class);
  }


}
