/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.pageobjects.sqs;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Frame;
import com.sqs.web.elements.Table;

import org.openqa.selenium.By;

import lombok.Getter;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Vacancies Page Object
 */
public class VacanciesPage extends PageObject {

  private final Button searchButton = new Button(By.xpath("//input[@value='Search']"));
  @Getter
  private final Table
      vacanciesTable =
      new Table(By.xpath("//table[@class='iCIMS_JobsTable iCIMS_Table']"));
  private final Frame frame = new Frame(By.xpath("//iframe[@id='icims_content_iframe']"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public VacanciesPage() {
  }

  public void onPage() {
    frame.switchToFrame();
    searchButton.isDisplayed();
  }

  @Step("Search For Vacancies")
  public VacanciesPage searchForVacancies() {
    searchButton.click();
    return this;
  }
}
