/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.pageobjects.sqs;

import org.openqa.selenium.By;

import ru.yandex.qatools.allure.annotations.Step;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Hyperlink;

/**
 * The Career Portal Page Object
 */
public class SqsCareerPortalPage extends PageObject {

  private final Hyperlink ireland = new Hyperlink(By.xpath("//a[.='Ireland']"));

  /**
   * Instantiates a new Sqs career portal page
   */
  public SqsCareerPortalPage() {
  }

  public void onPage() {
    ireland.isPresent();
  }

  @Step("Navigating to Ireland Careers page")
  public CareerIrelandPage navigateToIrelandCareersPage() {
    ireland.click();
    return navigatingTo(CareerIrelandPage.class);
  }
}
