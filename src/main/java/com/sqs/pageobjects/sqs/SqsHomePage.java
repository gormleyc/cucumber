/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.pageobjects.sqs;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.webdriver.DriverProvider;

import org.openqa.selenium.By;

import lombok.Getter;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The SQS Home Page Object
 */
public class SqsHomePage extends PageObject {

  @Getter
  private final String URL = "http://www.sqs.com";
  @Getter
  private final Hyperlink sqsLogo = new Hyperlink(By.xpath("//h2[.='Specialist Consultancy']"));
  public TopNavigationBar navBar; // this is composition.

  /**
   * Instantiates a new Sqs home page.
   */
  public SqsHomePage() {
  }

  public void onPage() {
    sqsLogo.isDisplayed();
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  @Step("Navigating to homepage: " + URL)
  public SqsHomePage init() {
    DriverProvider.getDriver().get(URL);
    //Ensure we are on the correct page
    onPage();
    // instantiate composite object
    this.navBar = new TopNavigationBar();
    return this;
  }

  /**
   * Navigate to page object.
   *
   * @return the page object
   */
  
  public SqsCareerPortalPage navigateToCareersPage() {
	navBar = new TopNavigationBar();
    return navBar.navigateToCareers();
  }

}
