/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.stepdefinitions;

import ru.yandex.qatools.allure.annotations.Step;

import com.sqs.pageobjects.sqs.VacanciesPage;

import cucumber.api.java.en.Then;

/**
 * Vacancies Step Page. (Example of Cucumber Steps page. Annotations for Allure
 * need added until listener implemented.)
 * 
 * @author gormleyc
 *
 */
public class VacanciesSteps {

	public VacanciesPage vacanciesPage;

	/**
	 * Search for Vacancies Step
	 * 
	 * @throws Throwable
	 */
	@Then("^Search for Vacancies$")
	@Step("Search for Vacancies")
	public void i_Navigate_to_Vacancies_Page() throws Throwable {
		vacanciesPage = new VacanciesPage();
		vacanciesPage.searchForVacancies();
	}
}