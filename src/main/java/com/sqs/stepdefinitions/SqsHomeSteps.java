/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.stepdefinitions;

import ru.yandex.qatools.allure.annotations.Step;

import com.sqs.pageobjects.sqs.SqsHomePage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * SqsHome Step Page. (Example of Cucumber Steps page. Annotations for Allure
 * need added until listener implemented.)
 * 
 * @author gormleyc
 *
 */
public class SqsHomeSteps {

	public SqsHomePage homePage;

	/**
	 * Navigate to SQS Home Page
	 * 
	 * (We need to move the initialisation of the browser to before the test)
	 * 
	 * @throws Throwable
	 */
	@Given("^I open SQS$")
	@Step("I open SQS")
	public void i_open_SQS() throws Throwable {		
		homePage = new SqsHomePage();
		homePage.init();
	}

	/**
	 * Navigate to Careers Step
	 * 
	 * @throws Throwable
	 */
	@Then("^I Navigate to Careers Page$")
	@Step("I Navigate to Careers Page")
	public void i_Navigate_to_Careers_Page() throws Throwable {
		homePage = new SqsHomePage();
		homePage.navigateToCareersPage();
	}
}