/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.stepdefinitions;

import ru.yandex.qatools.allure.annotations.Step;

import com.sqs.pageobjects.sqs.CareerIrelandPage;

import cucumber.api.java.en.Then;

/**
 * CareersIreland Step Page. (Example of Cucumber Steps page. Annotations for Allure
 * need added until listener implemented.)
 * 
 * @author gormleyc
 *
 */
public class CareersIrelandSteps {

	public CareerIrelandPage careerIrelandPage;

	/**
	 * Navigate to Vacancies Step
	 * 
	 * @throws Throwable
	 */
	@Then("^I Navigate to Vacancies Page$")
	@Step("I Navigate to Vacancies Page")
	public void i_Navigate_to_Vacancies_Page() throws Throwable {
		careerIrelandPage = new CareerIrelandPage();
		careerIrelandPage.navigateToVacanciesPage();
	}
}