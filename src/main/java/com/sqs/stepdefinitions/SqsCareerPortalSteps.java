/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.stepdefinitions;

import ru.yandex.qatools.allure.annotations.Step;

import com.sqs.pageobjects.sqs.SqsCareerPortalPage;

import cucumber.api.java.en.Then;

/**
 * SqsCareerPortal Step Page. (Example of Cucumber Steps page. Annotations for Allure
 * need added until listener implemented.)
 * 
 * @author gormleyc
 *
 */
public class SqsCareerPortalSteps {

	public SqsCareerPortalPage sqsCareerPortalPage;

	/**
	 * Navigate to Career Portal Page Step
	 * 
	 * @throws Throwable
	 */
	@Then("^I Navigate to Career Portal Page$")
	@Step("I Navigate to Career Portal Page")
	public void i_Navigate_to_Career_Portal_Page() throws Throwable {
		sqsCareerPortalPage = new SqsCareerPortalPage();
		sqsCareerPortalPage.navigateToIrelandCareersPage();
	}
}