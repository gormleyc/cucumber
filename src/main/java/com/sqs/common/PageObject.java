/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.common;


import static org.hamcrest.MatcherAssert.assertThat;

import com.sqs.core.sqslibs.WebLog;

import org.hamcrest.Matcher;

import ru.yandex.qatools.allure.annotations.Step;

/**
 * Base page object.
 */
public abstract class PageObject {

  /**
   * method that instantiates a new page object
   * and verifies the page has loaded
   *
   * @param c   the page object class to instantiate
   * @param <D> the page object class to instantiate
   * @return the page object class
   */
  public <D extends PageObject> D navigatingTo(Class<D> c) {
    D newPage = null;
    try {
      WebLog.info("Navigating to page: '" + c.getSimpleName() + "'");
      newPage = c.newInstance();
      //Ensure we are on the correct page
      newPage.onPage();
    } catch (Exception e) {
      WebLog.info("Unable to navigate to page: '" + c.getSimpleName() + "' due to exception: " + e
          .toString());
    }
    return newPage;
  }

  @Step("Asserting that element: {0}")
  public <T> void assertThatElement(String info, T actual, Matcher<? super T> matcher) {
    assertThat(info, actual, matcher);
  }

  public abstract void onPage();


}
