/*Copyright 2016 SQS
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
 */
package com.sqs.test;

import com.sqs.core.sqslibs.WebLog;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;

/**
 * TestNG setup class that acts as a test runner.
 * 
 * Annotations can be setup to execute features, suites, steps etc...
 * 
 * Recommend writing utility class to override CucumberOptions annotations to
 * remove the need for multiple runner classes.
 * 
 * @author gormleyc
 *
 */
@CucumberOptions(monochrome = true, features = "src/test/resources/features", glue = { "com.sqs.stepdefinitions" }, plugin = {
		"pretty", "html:target/cucumber-html-report",
		"json:target/cucumber1.json" }, tags = { "@Vacancies" })
public class TestNgBaseTest extends AbstractTestNGCucumberTests {
	/**
	 * Pre test setup. Starting driver, logging etc...
	 */
	@Before
	public void startUp() {
		WebLog.startTestCase("Start");
	}

	/**
	 * clean up after tests.
	 */
	@After
	public void cleanUp() {
		WebLog.endTestCase("End");
	}
}
